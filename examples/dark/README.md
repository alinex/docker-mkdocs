# Dark Theme Example

This directory contains a skeleton for using mkdocs. You can directly download all these files in your project directory and begin writing fantastic documentation.

All you have to do is:

-   Download [this directory](https://gitlab.com/alinex/docker-mkdocs/-/archive/master/docker-mkdocs-master.zip?path=examples/dark).
-   Extract the files into your project directory.
-   Edit `mkdocs.yml` and add site_name, description, author, copyright and enable navigation, social links or pdf creation.
-   Edit `docs/manifest.webmanifest` to update name and description.
-   Edit `docs/*.md` and add markdown pages.

And to create the documentation in the `site` directory only call `bin/docs` which will call the docker image to transform the markdown into HTML and if enabled create a PDF.

!!! note

    It is also ready configured to update documentation in GitLab pages if stored in GitLab is used.

_Alexander Schilling_
