# Last Changes

## Version 1.1.3 (29.11.2021)

-   reduce image size to 583MB and add more labels
-   switch to mkdocs-material 8.x

This is a breaking change (only found out after release) that means you have to change:

=== "mkdocs.yml new"

    markdown_extensions:
    - pymdownx.tabbed:
        alternate_style: true

=== "mkdocs.yml old"

    markdown_extensions:
    - pymdownx.tabbed

-   Bugfix 1.1.4 (30.11.2021) - fix call in gitlab without /data mounting

## Version 1.1.0 (19.11.2021)

-   initial release
-   with progress chart
-   with extended diagrams
-   with chart.js example inclusion

-   Bugfix 1.1.2 (22.11.2021) - reduce image size to 683MB

{!docs/assets/abbreviations.txt!}
