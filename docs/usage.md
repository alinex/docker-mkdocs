title: Usage

# Using MkDocs

This should give you a quick jump into making your own static documentation with MkDocs.

For a complete description please read https://alinex.gitlab.io/env/mkdocs.html.

## Installation

You need a working docker service on your host.

=== "Arch Linux"

    ```bash
    # install docker
    sudo pacman -Sy docker
    # configuration
    sudo gpasswd -a operator docker # allow operator to use docker without sudo
    # start and enable service
    sudo systemctl start docker.service
    sudo systemctl enable docker.service
    ```

=== "Debian/Ubuntu"

    ```bash
    # install prerequisites
    sudo apt update
    sudo apt install -y docker-ce docker-ce-cli
    # configuration
    sudo usermod -a -G docker operator # allow operator user to execute docker commands
    # start and enable docker service
    sudo systemctl start docker
    sudo systemctl enable docker.service
    ```

## Configuration

Use a preconfigured skeleton from the [examples](https://gitlab.com/alinex/docker-mkdocs/-/tree/master/examples) folder like or setup everything on your own like described in the following description.

First create a `docs` folder within your project there you write your documentation.

Now add the following files to it:

??? example "Example: Dark Theme"

    ```yaml title="mkdocs.yml"
    {% include "../examples/dark/mkdocs.yml" %}
    ```

    ```json title="docs/manifest.webmanifest"
    {% include "../examples/dark/docs/manifest.webmanifest" %}
    ```

    ```css title="docs/assets/extra.css"
    {% include "../examples/dark/docs/assets/extra.css" %}
    ```

    ```css title="docs/assets/pdf.css"
    {% include "../examples/dark/docs/assets/pdf.css" %}
    ```

    ```js title="docs/assets/extra.js"
    {% include "../examples/dark/docs/assets/extra.js" %}
    ```

Then update the file, especially the `nav` part in `mkdocs.yml`. You may also add `/site` to `.gitignore`, bacause this is the folder the output of the static site will be generated.

Also have a look at the [mkdocs](https://alinex.gitlab.io/env/mkdocs.html) description.

## Add Content

Now you should add all your documentation in the `docs/` folder als marksown pages. See the examples for [mkdocs markdown](https://alinex.gitlab.io/lang/mkdocs.html) to see what is possible.

## Generate HTML

You have to run it with the data binding of the directory on which to work, which is the current directory `$(pwd)`:

```bash
docker run -v $(pwd):/data alinex/mkdocs
```

It will then first create the HTML `site`, then the PDF within it. Afterwards you can copy this folder to your website.

### GitLab CI

Use the following CI script to run the document creation within GitLab CI and deploy to GitLab pages:

-   create documentation
-   deploy to pages

```yaml title=".gitlab-ci.yml"
{ % include "../examples/dark/.gitlab-ci.yml" % }
```

It also contains everything to build pdf files. ePub conversion is not included at the moment but may later be added if possible.

Or you can use [GitLab CI template](https://gitlab.com/alinex/gitlab-ci/-/blob/main/deploy/pages-mkdocs.md) for easy use.

{!docs/assets/abbreviations.txt!}
