title: Build

# Building MkDocs Image

### Manually

```bash
docker build -t alinex/mkdocs:test .
```

### GitLab CI

As Docker in Docker is not allowed at gitlab.com I use [kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html).

Therefore the following variables are defined

| Key                  | Value                       | Protected | Masked |
| -------------------- | --------------------------- | --------- | ------ |
| CI_REGISTRY          | https://index.docker.io/v1/ | no        | no     |
| CI_REGISTRY_IMAGE    | docker.io/alinex/mkdocs     | no        | no     |
| CI_REGISTRY_PASSWORD | xxxxxxxxxxxxxx              | no        | yes    |
| CI_REGISTRY_USER     | alinex                      | no        | no     |

The `.gitlab-ci.yml` will build the docker file and upload it to https://hub.docker.com.

## Content of Docker

- python3
- mkdocs
- mkdocs-material
- pymdown-extensions
- markdown-blockdiag
- markdown-include
- mkdocs-include-markdown-plugin
- mkdocs-with-pdf
- django-weasyprint
- mkdocs-awesome-pages-plugin
- mkdocs-minify-plugin
- mkdocs-git-revision-date-localized-plugin

The whole process is run by the `/run.sh` script.

{!docs/assets/abbreviations.txt!}
