title: Overview

# MkDocs Build Environment

This is a preset mkdocs docker image with all extensions to build doc pages with the possibilities described under [https://alinex.gitlab.io/env/mkdocs.html](https://alinex.gitlab.io/env/mkdocs.html).

It also contains everything to build pdf files. only ePub conversion is not included at the moment but may later be added if possible.

{!docs/assets/stats-pdf-license.txt!}

{!docs/assets/abbreviations.txt!}
