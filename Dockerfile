FROM python:slim
ARG BUILD_DATE
ARG BUILD_VERSION

LABEL maintainer="Alexander Schilling <info@alinex.de>"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="alinex/mkdocs"
LABEL org.label-schema.description="This is a setup ready to create mkdocs documentation with all the extensions described under https://alinex.gitlab.io/env/mkdocs.html"
LABEL org.label-schema.url="https://alinex.gitlab.io/docker-mkdocs/"
LABEL org.label-schema.vcs-url="https://gitlab.com/alinex/docker-mkdocs"
#LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vendor="Alinex"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.docker.cmd="docker run -v \$(pwd):/data alinex/mkdocs"

RUN apt-get update && apt-get install -y git libglib2.0-dev libpango1.0-dev \
    && apt-get clean \
    && rm -rf /usr/share/doc/ /usr/share/man/ /usr/share/locale/
RUN pip install --no-cache-dir mkdocs \
    && pip install --no-cache-dir mkdocs-material \
    && pip install --no-cache-dir pymdown-extensions \
    && pip install --no-cache-dir markdown-blockdiag \
    && pip install --no-cache-dir markdown-include \
    && pip install --no-cache-dir mkdocs-include-markdown-plugin \
    && pip install --no-cache-dir mkdocs-awesome-pages-plugin \
    && pip install --no-cache-dir mkdocs-minify-plugin \
    && pip install --no-cache-dir mkdocs-git-revision-date-localized-plugin \
    && pip install --no-cache-dir django-weasyprint \
    && pip install --no-cache-dir mkdocs-with-pdf

# epub
#RUN apt-get update && apt-get install -y calibre && apt-get clean

COPY /run.sh /
VOLUME ["/data"]
CMD ["/run.sh"]
