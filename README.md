# MkDocs Build Environment

Preset mkdocs docker image with all extensions to build doc pages with the possibilities described under https://alinex.gitlab.io/env/mkdocs.

It also contains everything to build pdf files. ePub conversion is not included at the moment but may later be added if possible.

## Usage

You have to run it with the data binding of the directory on which to work, which is the current directory `$(pwd)`:

```bash
docker run -v $(pwd):/data alinex/mkdocs
```

It will then first create the HTML site, then the PDF.

A detailed description on how to do this can be found in the [manual](https://alinex.gitlab.io/docker-mkdocs).

### GitLab CI

Use the following CI script to run the document creation within GitLab CI and deploy to GitLab pages:

-   create documentation
-   deploy to pages

```yaml
image: alinex/mkdocs

pages:
    stage: deploy
    script:
        - "/run.sh"
        - rm -rf public
        - mv site public
    artifacts:
        paths:
            - public
```

## Documentation

Find a complete manual under [alinex.gitlab.io/node-core](https://alinex.gitlab.io/node-core).

If you want to have an offline access to the documentation, feel free to download the [PDF Documentation](https://alinex.gitlab.io/docker-mkdocs/alinex-mkdocs.pdf).

## License

(C) Copyright 2021 Alexander Schilling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> <https://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
