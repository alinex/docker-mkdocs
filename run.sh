#!/bin/bash

# switch to correct directory for build
[ -e "/data/mkdocs.yml" ] && cd /data # manual call with /data mounted 

if [ ! -e "mkdocs.yml" ]; then
    >&2 echo "Error: No $(pwd)/mkdocs.yml configuration found!" 
    exit 1
fi

mkdocs build

# epub generation buggy
#name=$(echo site/*.pdf | sed 's/\..*//')
#[ -s "$name.pdf" ] && ebook-convert $name.pdf $name.epub
#[ -e "$name.epub" ] # exit with success if file was created